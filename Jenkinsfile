pipeline {
    agent any

    stages {

        stage('With multiple SH block') {
            steps {
                // Using multiple sh blocks, variables will not be available with other sh blocks
                script {
                    // This is ok since set/export env var and use it within the same process
                    sh '''
                        export TEST=foobar
                        echo expect [foobar] but got [$TEST]
                    '''
                    // This will fail since the second 'sh' block is essentially a new process
                    sh '''
                        echo expect [foobar] but got [$TEST]
                    ''' 
                }
            }
        }

        stage('With withEnv block') {
            steps {
                // using nested withEnv, variables will be available within the withEnv scope even with multiple sh blocks
                withEnv(["TEST=${sh(returnStdout: true, script:'. ${WORKSPACE}/set-env.sh')}"]) {
                    withEnv(["PARAM1=${sh(returnStdout: true, script: 'echo $TEST | cut -d, -f1')}",
                        "PARAM2=${sh(returnStdout: true, script: 'echo $TEST | cut -d, -f2')}",
                        "PARAM3=${sh(returnStdout: true, script: 'echo $TEST | cut -d, -f3')}"]) {
                        script {
                            sh 'echo expect TEST=[foo bar baz] but got [$TEST]'
                            sh 'echo expect PARAM1=[foo] but got [$PARAM1]'
                            sh 'echo expect PARAM2=[bar] but got [$PARAM2]'
                            sh 'echo expect PARAM3=[baz] but got [$PARAM3]'
                        }
                    }
                }
            }
        }

        stage('With groovy script') {
            steps {
                // using groovy script
                script {
                    keys="${sh(returnStdout: true, script: '${WORKSPACE}/set-env.sh')}".split(',')
                }
                // then use withEnv with values from keys
                withEnv(["PARAM1=${keys[0]}","PARAM2=${keys[1]}","PARAM3=${keys[2]}"]) {
                    sh "env"
                    sh 'echo expect PARAM1=[ASIAXYLL...] but got [$PARAM1]'
                    sh 'echo expect PARAM2=[eEq52US8...] but got [$PARAM2]'
                    sh 'echo expect PARAM3=[IQoJb3Jp...] but got [$PARAM3]'
                }
            }
        }
    }
}